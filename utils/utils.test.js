const utils = require('./utils.js');
const expect = require('expect');
describe('Utils',() => {
  describe('#add',() => {
    it('should add two numbers',() => {
      var res  = utils.add(11,22);
      if(res!=33){
        throw new Error(`expected 33 but got ${res}`)
      }
    });
  });
  describe('#AsyncAdd',() => {
    it('should aysnc add two numbers',(done) => {
      utils.asyncAdd(3,4,(sum) => {
        expect(sum).toBe(7);
        done();
      });
    });
  });
});
describe('NameMatching',() => {
  it('should check if the names match',() => {
    var getName = utils.setName({},'Sriharsh Aditya');
    expect(getName).toEqual({firstName : 'Sriharsh',lastName : 'Aditya'});
  });
});
