const request = require('supertest');
const app = require('./server.js').app;
const expect = require('expect');
describe('server',() => {
  it('should check for a user',(done) => {
    request(app)
    .get('/')
    .expect(200)
    .expect((res) => {
      expect(res.body).toContainEqual({name : 'Sriharsh',age : 19});
    })
    .end(done);
  });
});
